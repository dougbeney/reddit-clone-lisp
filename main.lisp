(ql:quickload
 '(clack
   cl-json
   cl-ppcre
   parse-number))

(load "page-builder.lisp")

;;; Package definition

(defpackage :reddit-clone
  (:use :cl)
  (:export *clack-instance*
           start
           stop
           restart))

(in-package :reddit-clone)

;;; Server controls

(defvar *clack-instance* nil)

(defun stop ()
  "Stops server"
  (clack:stop *clack-instance*)
  (setq *clack-instance* nil))

(defun start ()
  "Starts or restarts server"
  (unless (eq *clack-instance* nil)
    (stop))
  (setq *clack-instance*
        (clack:clackup
         (lambda (env) (funcall 'web-request env)))))

;;; Load static pages

(defparameter help.txt (uiop:read-file-string "help.txt"))
(defparameter 404.txt (uiop:read-file-string "404.txt"))

;;; General helper functions

(defun string-to-property (string)
  "Takes a string, converts it to a :PROPERTY"
  (intern
   (string-upcase
    (cl-ppcre:regex-replace-all #\space string "-"))
   "KEYWORD"))

(defun string-to-number (string)
  "Converts a string to a number. Decimals allowed."
  (ignore-errors (parse-number:parse-number string)))

(defun querystring-to-plist (querystring)
  ;;; If no query strings, return nil
  (when (eq querystring nil)
    (return-from querystring-to-plist nil))
  ;;; First we use split-sequence to separate properties
  ;;; "name=Doug&age=20" --> ("name=Doug" "age=20")
  (let ((parsed-properties '())
        (properties (split-sequence:split-sequence #\& querystring)))
    (loop for prop in (reverse properties) do
         ;;; Next, we split the keyvalue at the '='.
         ;;; "name=Doug" --> ("name" "Doug")
         (let*
             ((keyvalue (split-sequence:split-sequence #\= prop))
              (key (format nil"~a" (car keyvalue)))
              (value (cadr keyvalue)))
           (cond
             ;;; if "true" or "t" use t (ignore case)
             ;;; if "false" or "nil" use nil (ignore case)
             ;;; if a number, use a number instead of a string.
             ((or (string-equal value "true")
                  (string-equal value "t")) (push t parsed-properties))
             ((or
               (string-equal value "false")
               (string-equal value "nil")) (push nil parsed-properties))
             ((string-to-number value) (push (string-to-number value) parsed-properties))
             (t (push value parsed-properties)))
           (push (string-to-property key) parsed-properties)))
    ;;; Return parsed values
    ;;; (:NAME "Doug" :AGE "20")
    parsed-properties))

;;; Web-related helper functions

(defmacro basic-response-code (code text)
  `(list ,code (list :content-type "text/plain") (list (page-builder:template ,text))))

(defun :200 (text)
  (basic-response-code 200 text))

(defun :404 (text)
  (basic-response-code 404 text))

(defun router (path query-plist)
  (print query-plist)
  (cond
    ;;; Home page
    ((string= path "/")
     (:200 "Plain-text reddit!"))

    ;;; Help page
    ((string= path "/help")
     (:200 help.txt))

    ;;; Page does not exist!
slime    (t
     (:404 404.txt))))

(defun web-request (env)
  (router
   (getf env :path-info)
   (querystring-to-plist
    (getf env :query-string))))
