;;; page-builder.lisp
;;; The purpose of this file is
;;; no provide plaint-text
;;; templating for web-pages.

(defpackage :page-builder
  (:use :cl)
  (:export template))

(in-package :page-builder)

;;; The navigation
(defun page--nav ()
    (format nil
     "Pages:~%/   /help   /create-account   /login~%~%"))

;;; Use a template and use 'content' as the body content
;;; The key, :nav, allows you to control the
;;; visibility of the nav with t and nil.
(defun template (content &key (nav t))
  (let ((text ""))
    (when nav
      (setq text (concatenate 'string text (page--nav))))
    (setq text (concatenate 'string text content))
    text))
